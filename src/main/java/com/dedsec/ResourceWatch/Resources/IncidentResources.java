package com.dedsec.ResourceWatch.Resources;

import lombok.Data;

@Data
public class IncidentResources
{
	private Long id;
	private String latitude;
	private String longitude;
	private String description;
}
