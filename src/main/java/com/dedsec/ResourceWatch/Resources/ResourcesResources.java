package com.dedsec.ResourceWatch.Resources;

import lombok.Data;

@Data
public class ResourcesResources
{
	private Long id;
	private Long cpf;
	private String name;
	private String function;
	private String latitude;
	private String longitude;
}
