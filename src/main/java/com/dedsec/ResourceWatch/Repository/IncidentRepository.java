package com.dedsec.ResourceWatch.Repository;

import java.util.List;

import org.springframework.data.repository.cdi.Eager;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dedsec.ResourceWatch.Entity.Incident;

@Eager
public interface IncidentRepository extends JpaRepository<Incident, Long>
{
	@Override
	List<Incident> findAll();
}
