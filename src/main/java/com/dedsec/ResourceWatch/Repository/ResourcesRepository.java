package com.dedsec.ResourceWatch.Repository;

import org.springframework.data.repository.cdi.Eager;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dedsec.ResourceWatch.Entity.Resources;

@Eager
public interface ResourcesRepository extends JpaRepository<Resources, Long>
{
	Resources findByCpf(Long cpf);
}
