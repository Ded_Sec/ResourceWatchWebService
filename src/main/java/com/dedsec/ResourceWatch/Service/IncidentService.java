package com.dedsec.ResourceWatch.Service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import com.dedsec.ResourceWatch.Entity.Incident;
import com.dedsec.ResourceWatch.Mapper.IncidentMapper;
import com.dedsec.ResourceWatch.Resources.IncidentResources;
import com.dedsec.ResourceWatch.Repository.IncidentRepository;

@RestController
@RequestMapping("{url-base}/incident")
public class IncidentService
{
	@Autowired
	private IncidentRepository incidentRepository;


	@PostMapping("/save")
	public Object save(@RequestBody IncidentResources entity, HttpServletRequest request)
	{
		try
		{
			Incident incident = new Incident();

			incident.setId(entity.getId());
			incident.setLatitude(entity.getLatitude());
			incident.setLongitude(entity.getLongitude());
			incident.setDescription(entity.getDescription());

			incidentRepository.saveAndFlush(incident);

			return IncidentMapper.getIncidentMapper(incident);
		}
		catch (Exception e)
		{
			return e.getMessage();
		}
	}

	@GetMapping("/get")
	public Object get(HttpServletRequest request)
	{
		try
		{
			return incidentRepository.findAll();
		}
		catch (Exception e)
		{
			return e.getMessage();
		}
	}
}
