package com.dedsec.ResourceWatch.Service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import com.dedsec.ResourceWatch.Mapper.ResourcesMapper;
import com.dedsec.ResourceWatch.Repository.ResourcesRepository;

@RestController
@RequestMapping("{url-base}/resources")
public class ResourcesService
{
	@Autowired
	private ResourcesRepository resourcesRepository;

	@GetMapping("/find/{cpf}")
	public Object findByCpf(@PathVariable("cpf") Long cpf, HttpServletRequest request)
	{
		try
		{
			return ResourcesMapper.getResourcesMapper(resourcesRepository.findByCpf(cpf));
		}
		catch (Exception e)
		{
			return e.getMessage();
		}
	}
}
