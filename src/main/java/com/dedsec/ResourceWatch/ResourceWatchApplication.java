package com.dedsec.ResourceWatch;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Configuration
@ComponentScan
@SpringBootApplication
public class ResourceWatchApplication
{
	public static void main(String[] args)
	{
		SpringApplication.run(ResourceWatchApplication.class, args);
	}
}
