package com.dedsec.ResourceWatch.Entity;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.NoArgsConstructor;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "resources")
@ToString(of = "id")
public class Resources implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "cpf")
	private Long cpf;

	@Column(name = "name")
	private String name;

	@Column(name = "function")
	private String function;

	@Column(name = "latitude")
	private String latitude;

	@Column(name = "longitude")
	private String longitude;
}
