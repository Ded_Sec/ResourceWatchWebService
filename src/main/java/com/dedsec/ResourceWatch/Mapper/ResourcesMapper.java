package com.dedsec.ResourceWatch.Mapper;

import java.util.HashMap;

import com.dedsec.ResourceWatch.Entity.Resources;

public class ResourcesMapper
{
	public static Object getResourcesMapper(Resources resources)
	{
		HashMap<String, Object> map = new HashMap<>();

		map.put("id", resources.getId());
		map.put("cpf", resources.getCpf());
		map.put("name", resources.getName());
		map.put("function", resources.getFunction());
		map.put("latitude", resources.getLatitude());
		map.put("longitude", resources.getLongitude());

		return map;
	}
}
