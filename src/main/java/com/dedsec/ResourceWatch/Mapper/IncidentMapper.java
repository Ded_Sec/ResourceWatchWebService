package com.dedsec.ResourceWatch.Mapper;

import java.util.HashMap;

import com.dedsec.ResourceWatch.Entity.Incident;

public class IncidentMapper
{
	public static Object getIncidentMapper(Incident incident)
	{
		HashMap<String, Object> map = new HashMap<>();

		map.put("id", incident.getId());
		map.put("latitude", incident.getLatitude());
		map.put("longitude", incident.getLongitude());
		map.put("descricao", incident.getDescription());

		return map;
	}
}
